## Repository for Lecture Slides of Introduction to Digital Humanities

### WS 2016/2017

* [1. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/61e17abd40b1ddc03a8aa5abc88eb23cf08fc0e3/IntroDH_20161010.pdf)
* [2. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161017.pdf)